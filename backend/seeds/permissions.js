const PERMISSIONS = require('../src/permissions');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('permissions').del().then(function() {
    // Inserts seed entries
    const rows = PERMISSIONS.map((p, idx) => ({ id: idx + 1, name: p }));
    return knex('permissions').insert(rows);
  });
};
