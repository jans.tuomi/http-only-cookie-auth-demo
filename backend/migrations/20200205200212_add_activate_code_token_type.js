exports.up = function(knex) {
  return knex.schema.raw(`
    ALTER TABLE "tokens"
    DROP CONSTRAINT "tokens_type_check",
    ADD CONSTRAINT "tokens_type_check"
    CHECK (type IN ('session', 'activation'))
  `);
};

exports.down = function(knex) {
  return knex.schema.raw(`
    ALTER TABLE "tokens"
    DROP CONSTRAINT "tokens_type_check",
    ADD CONSTRAINT "tokens_type_check"
    CHECK (type IN ('session'))
  `);
};
