exports.up = function(knex) {
  return knex.schema.createTable('tokens', function(table) {
    table.increments();
    table.integer('user_id').unsigned().notNullable();
    table.foreign('user_id').references('users.id');
    table.enu('type', [ 'session' ]).notNullable();
    table.string('value').notNullable();
    table.timestamps(true, true);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('tokens');
};
