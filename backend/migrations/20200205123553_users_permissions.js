exports.up = function(knex) {
  return knex.schema.createTable('users_permissions', function(table) {
    table.integer('user_id').unsigned().references('users.id').notNullable();
    table.integer('permission_id').unsigned().references('permissions.id').notNullable();
    table.primary([ 'user_id', 'permission_id' ]);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('users_permissions');
};
