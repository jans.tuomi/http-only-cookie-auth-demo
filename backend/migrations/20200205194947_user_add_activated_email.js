exports.up = function(knex) {
  return knex.schema.table('users', function(table) {
    table.string('email').notNullable().default('');
    table.boolean('activated').notNullable().default(false);
  });
};

exports.down = function(knex) {
  return knex.schema.table('users', function(table) {
    table.dropColumn('email');
    table.dropColumn('activated');
  });
};
