const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const routes = require('./routes');

const app = express();

app.use((_, res, next) => {
  try {
    next();
  } catch (err) {
    console.error(err);
    res.status(500);
    res.json({
      error: 'Internal server error',
    });
  }
});
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use((req, res, next) => {
  res.set('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.set('Access-Control-Allow-Headers', 'Content-Type');
  res.set('Access-Control-Allow-Credentials', true);
  next();
});
app.use(routes.normalRouter);
app.use(routes.authedRouter);

const port = 4000;
app.listen(port, () => console.log(`App listening on http://localhost:${port}!`));
