const consoleBackend = {
  send: ({ recipients, subject, body, cc, bcc }) => {
    if (!recipients) throw new Exception('Recipients missing');
    if (!subject) throw new Exception('Subject missing');

    console.log('Email sent to console.');
    console.log('=== To:', recipients.join(', '));
    if (cc) console.log('=== CC:', cc.join(', '));
    if (bcc) console.log('=== BCC:', bcc.join(', '));
    console.log('=== Subject:', subject);
    console.log();
    console.log('=== Body:');
    console.log(body);
    console.log('===');
  },
};

module.exports = {
  consoleBackend,
};
