const express = require('express');

const db = require('./db');
const { permissionMiddleware } = require('./authz');
const PERMISSIONS = require('./permissions');

const { authMiddleware, registerRoute, loginRoute, logoutRoute, activateRoute } = require('./authn');

const normalRouter = express.Router();
const authedRouter = express.Router();

authedRouter.use(authMiddleware);

normalRouter.get('/', (req, res) => {
  res.json({
    message: 'Backend API',
  });
});

/* Authentication flow */
normalRouter.post('/register', registerRoute);
normalRouter.post('/login', loginRoute);
normalRouter.post('/activate', activateRoute);
authedRouter.post('/logout', logoutRoute);

authedRouter.get('/user', (req, res) => {
  const user = req.user;
  return res.json({
    message: `Hello user ${user.username}!`,
    username: user.username,
  });
});

authedRouter.get('/users', permissionMiddleware(PERMISSIONS.READ_USERS), async (req, res) => {
  const users = await db('users').select('id', 'username', 'created_at');
  return res.json({
    users,
  });
});

module.exports = { authedRouter, normalRouter };
