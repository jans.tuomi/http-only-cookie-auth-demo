const db = require('./db');

const userHasPermission = async (user, permission_name) => {
  const permission = await db('permissions').where({ name: permission_name }).first();
  const userPermission = await db('users_permissions')
    .where({ user_id: user.id, permission_id: permission.id })
    .first();

  return !!userPermission;
};

const userHasPermissions = async (user, permission_names) => {
  for (permission_name of permission_names) {
    const hasPermission = await userHasPermission(user, permission_name);
    if (!hasPermission) {
      return false;
    }
  }
  return true;
};

const permissionMiddleware = permission_name => async (req, res, next) => {
  const user = req.user;
  const hasPermission = Array.isArray(permission_name)
    ? await userHasPermissions(user, permission_name)
    : await userHasPermission(user, permission_name);

  if (hasPermission) {
    await next();
  } else {
    res.status(401);
    res.json({
      error: 'Required permission missing.',
      permission_name,
    });
  }
};

module.exports = {
  userHasPermission,
  permissionMiddleware,
};
