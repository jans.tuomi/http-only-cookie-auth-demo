import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { navigate } from '@reach/router';

import axios from './axios';
import userState from './UserState';

const Register = () => {
  const { register, handleSubmit } = useForm();
  const [ data, setData ] = useState({});

  const onSubmit = async data => {
    if (data.password !== data.confirm_password) {
      return setData({ errors: [ 'Passwords do not match.' ] });
    }
    try {
      const resp = await axios.post('/register', data);
      const { username } = resp.data;
      userState.authError = null;
      userState.username = username;
      setData({ submitted: true });
    } catch (err) {
      if (err.response && err.response.data) {
        setData({
          errors: [ err.response.data.error, ...(err.response.data.errors || []) ],
        });
      } else {
        console.error(err);
        setData({ errors: [ 'Unknown error occurred. See console for details.' ] });
      }
    }
  };

  if (data.submitted) {
    return <h1>You should be receiving an account activation email shortly.</h1>;
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1 className="title">Register new account</h1>
      <label className="label">
        Username <input className="input" name="username" type="text" ref={register} required />
      </label>
      <label className="label">
        Email <input className="input" name="email" type="email" ref={register} required />
      </label>
      <label className="label">
        Password <input className="input" name="password" type="password" ref={register} required />
      </label>
      <label className="label">
        Confirm password <input className="input" name="confirm_password" type="password" ref={register} required />
      </label>
      <button className="button" type="submit">
        Register
      </button>
      <ul className="error">{data.errors ? data.errors.map(e => <li>{e}</li>) : null}</ul>
    </form>
  );
};

export default Register;
