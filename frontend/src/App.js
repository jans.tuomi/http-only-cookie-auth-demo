import React, { useEffect } from 'react';
import { Router } from '@reach/router';

import Login from './Login';
import Register from './Register';
import Home from './Home';
import userState from './UserState';
import './App.css';
import axios from './axios';
import Activate from './Activate';

const LoggedInRoutes = props => {
  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const resp = await axios.get('/user');
        const { username } = resp.data;
        userState.username = username;
      } catch (err) {
        userState.authError = err;
        console.error(err);
      }
    };
    fetchUserInfo();
  }, []);

  return props.children;
};

const App = () => {
  const fetchUserInfo = async () => {
    try {
      const resp = await axios.get('/user');
      const { username } = resp.data;
      userState.username = username;
    } catch (err) {
      userState.authError = err;
      console.error(err);
    }
  };
  fetchUserInfo();
  return (
    <div className="container">
      <Router>
        <Login path="/login" />
        <Register path="/register" />
        <Activate path="/activate/:activationToken" />
        <Home path="/" exact />
      </Router>
    </div>
  );
};

export default App;
