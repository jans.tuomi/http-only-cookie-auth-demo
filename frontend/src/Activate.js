import React, { useEffect, useState } from 'react';
import axios from './axios';
import { Link } from '@reach/router';

import userState from './UserState';

const Activate = ({ user, activationToken }) => {
  const [ data, setData ] = useState({});
  useEffect(() => {
    (async () => {
      try {
        const resp = await axios.post('/activate', { activationToken });
        const { username } = resp.data;
        user.authError = null;
        user.username = username;
        setData({
          message: 'Account succesfully activated. You are now logged in.',
        });
      } catch (err) {
        setData({
          error: err.response.data.error,
        });
      }
    })();
  }, []);

  return (
    <div>
      <h2 className="subtitle">Activating account</h2>
      <div>
        <div className="error" key="error">
          {data.error}
        </div>
        <div>{data.message}</div>
        {data.message && <Link to="/">To front page</Link>}
      </div>
    </div>
  );
};

export default props => <Activate user={userState} {...props} />;
