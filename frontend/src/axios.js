import axios from 'axios';
import { navigate } from '@reach/router';

axios.defaults.baseURL = process.env.REACT_APP_API_BASE_URL || 'http://localhost:4000';
axios.defaults.withCredentials = true;
axios.defaults.timeout = 2000;

axios.interceptors.response.use(
  response => {
    // Do something with response data
    return response;
  },
  err => {
    // Do something with response error
    if (err.response && err.response.status === 403) {
      navigate(`/login`);
    }
    return Promise.reject(err);
  },
);

export default axios;
