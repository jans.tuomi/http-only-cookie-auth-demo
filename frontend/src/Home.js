import React from 'react';
import { observer } from 'mobx-react';

import LogoutBtn from './LogoutBtn';
import userState from './UserState';
import UserList from './UserList';

const Home = observer(({ user }) => {
  if (user.authError) {
    const err = user.authError;
    const text = (err.response && err.response.data.message) || String(err);
    return <div className="error">{text}</div>;
  }

  if (!user.username) {
    return 'Loading...';
  }

  return (
    <div>
      <div className="navbar">
        <LogoutBtn />
      </div>

      <h1 className="title">Hello {user.username}!</h1>
      <div>
        <img src="https://placehold.it/200x200" alt="Placeholder" />
      </div>
      <div>
        <UserList />
      </div>
    </div>
  );
});

export default () => <Home user={userState} />;
