import React, { useEffect, useState } from 'react';
import axios from './axios';

const Table = ({ users }) => {
  const userRows = !!users
    ? users.map(user => (
        <tr key={user.id}>
          <td>{user.id}</td>
          <td>{user.username}</td>
        </tr>
      ))
    : null;

  return (
    <table className="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Username</th>
        </tr>
      </thead>
      <tbody>{userRows}</tbody>
    </table>
  );
};

const UserList = () => {
  const [ data, setData ] = useState({});
  useEffect(() => {
    (async () => {
      try {
        const resp = await axios.get('/users');
        setData({
          users: resp.data.users,
        });
      } catch (err) {
        if (err.response.status === 401) {
          setData({
            error: err.response.data.error,
          });
        } else {
          throw err;
        }
      }
    })();
  }, []);

  return (
    <div>
      <h2 className="subtitle">User list</h2>
      <div>
        <div className="error" key="error">
          {data.error}
        </div>
        {!data.error && <Table users={data.users} />}
      </div>
    </div>
  );
};

export default UserList;
