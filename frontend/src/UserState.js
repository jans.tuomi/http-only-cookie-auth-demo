import { observable, decorate } from 'mobx';

class UserState {
  username = null;
  authError = false;
}

decorate(UserState, {
  username: observable,
  authError: observable,
});

const userState = new UserState();
export default userState;
