import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { navigate, Link } from '@reach/router';

import axios from './axios';
import userState from './UserState';

const Login = () => {
  const { register, handleSubmit } = useForm();
  const [ data, setData ] = useState({});

  const onSubmit = async data => {
    try {
      const resp = await axios.post('/login', data);
      const { username } = resp.data;
      userState.authError = null;
      userState.username = username;
      navigate('/');
    } catch (err) {
      if (err.response && err.response.data) {
        setData({ error: err.response.data.error });
      } else {
        console.error(err);
        setData({ error: 'Unknown error occurred. See console for details.' });
      }
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1 className="title">Log in</h1>
      <label className="label">
        Username <input className="input" name="username" type="text" ref={register} required />
      </label>
      <label className="label">
        Password <input className="input" name="password" type="password" ref={register} required />
      </label>
      <button className="button" type="submit">
        Log in
      </button>
      <div className="error">{data.error}</div>
      <div>
        No account? Register <Link to="/register">here</Link>.
      </div>
    </form>
  );
};

export default Login;
