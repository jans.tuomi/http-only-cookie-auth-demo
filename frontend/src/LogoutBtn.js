import React from 'react';
import { navigate } from '@reach/router';
import { observer } from 'mobx-react';

import axios from './axios';
import userState from './UserState';

const LogoutBtn = observer(({ user }) => {
  const doLogout = async () => {
    await axios.post('/logout');
    user.username = null;
    navigate('/login');
  };

  return (
    <button onClick={doLogout} type="button">
      Log out
    </button>
  );
});

export default () => <LogoutBtn user={userState} />;
